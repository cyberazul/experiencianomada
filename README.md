# API - Experiencia Nómada (Viajes Recomendados)

Se trata de un portal llamado **Experiencia Nómada** donde los usuarios registrados pueden compartir experiencias de viajes y recomendar ubicaciones únicas y diferentes a las habituales.

Las funcionalidades difieren entre _usuarios anónimos_ y _usuarios registrados_.

Los _usuarios anónimos_ pueden ver las recomendaciones, realizar búsquedas por lugar y categoría, ver el número de votos de cada recomendación y organizar los resultados por votos. También pueden registrarse gratuitamente.

Los _usuarios registrados_, una vez hacen login, además de todo lo que pueden hacer los _usuarios anónimos_, pueden añadir sus propias recomendaciones (incluyendo una foto), votar y comentar las recomendaciones de otros usuarios.

## Instalar backend

1. Crear una base de datos en un MySQL local con el código guardado en `db/ExperienciaNomada.sql`.
2. Instalar las dependencias mediante el comando `npm install` (o `npm i`).
3. Guardar el archivo `.env.example` como `.env` y cumplimentar los datos necesarios.
4. Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

## Instalar frontend

1. Instalar las dependencias mediante el comando `npm install` (o `npm i`).
2. Crear un archivo `.env.local` en la carpeta _frontend_ y agregar la variable `VITE_API_URL=` y asegurarse de que almacene la URL de la API. Por ejemplo: VITE_API_URL=http://localhost:3000
3. Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

## Entidades

### Comment

| Campo             | Tipo     | Descripción                                            |
| ----------------- | -------- | ------------------------------------------------------ |
| id                | INT      | Identificador único del comentario                     |
| Users_id          | INT      | Identificador único del usuario que creó el comentario |
| recommendation_id | INT      | Identificador único de la recomendación                |
| comment           | VARCHAR  | Texto del comentario                                   |
| createAt          | DATETIME | Fecha y hora de creación del comentario                |
| vote              | INT      | Voto del comentario                                    |

### Recommendation

| Campo          | Tipo     | Descripción                                               |
| -------------- | -------- | --------------------------------------------------------- |
| id             | INT      | Identificador único de la recomendación                   |
| Users_id       | INT      | Identificador único del usuario que creó la recomendación |
| title          | VARCHAR  | Título de la recomendación                                |
| place          | VARCHAR  | Lugar al que se refiere la recomendación                  |
| category       | VARCHAR  | Categoría de viaje al que se refiere la recomendación     |
| recommendation | VARCHAR  | Texto de la recomendación                                 |
| photo          | VARCHAR  | Imagen de la recomendación                                |
| createAt       | DATETIME | Fecha y hora de creación de la recomendación              |

### Users

| Campo    | Tipo     | Descripción                          |
| -------- | -------- | ------------------------------------ |
| id       | INT      | Identificador único del usuario      |
| name     | VARCHAR  | Nombre del usuario                   |
| email    | VARCHAR  | Email del usuario                    |
| password | VARCHAR  | Contraseña del usuario               |
| createAt | DATETIME | Fecha y hora de creación del usuario |
| avatar   | VARCHAR  | Avatar del usuario                   |

## Colección de endpoints

### Usuarios:

- POST `/usuario/registro` - Registro de usuarios.
- POST `/usuario/loguear` - Login de usuarios.
- POST `/usuario/perfil` - Actualización de perfil de usuario añadiendo un avatar.
- GET `/usuario` - Retornar información del usuario del token.

### Recomendaciones:

- POST `/recomendaciones` - Creación de una recomendación.
- POST `/recomendaciones/comentarios/:id` - Comentar una recomendación.
- GET `/recomendaciones` - Listado de recomendaciones, que también filtra las recomendaciones por lugar y categoría a través de query params.
- GET `/recomendaciones/ascendientes` - Ordenar recomendaciones por votos de manera ascendente.
- GET `/recomendaciones/descendientes` - Ordenar recomendaciones por votos de manera descendente.
- GET `/recomendaciones/:id` - Visualización de una recomendación en detalle.
- DELETE `/recomendaciones/:id` - Eliminación de una recomendación por parte del usuario que la creó.
