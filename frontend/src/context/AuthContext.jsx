// Importamos los hooks.
import { createContext, useState, useEffect } from 'react';

// Importamos la URL base de nuestra API.
const apiUrl = import.meta.env.VITE_API_URL;

// Creamos un contexto de autenticación.
const AuthContext = createContext();

// Definimos un proveedor de autenticación.
export const AuthProvider = ({ children }) => {
    // Estado para almacenar el token del usuario.
    const [token, setToken] = useState(localStorage.getItem('token') || null);
    // Estado para almacenar la información del usuario.
    const [user, setUser] = useState(null);

    // Efecto que se ejecuta cuando cambia el token.
    useEffect(() => {
        // Función asincrónica para obtener la información del usuario.
        const fetchUser = async () => {
            try {
                const res = await fetch(`${apiUrl}/usuario`, {
                    headers: {
                        'x-access-token': token
                    }
                });

                const body = await res.json();

                if (body.status === 'error') {
                    throw new Error(body.message);
                }

                // Establecemos la información del usuario en el estado.
                setUser(body.data.user);
            } catch (err) {
                console.error(err);
            }
        };

        // Si hay un token, llamamos a la función para obtener la información del usuario.
        if (token) fetchUser();
    }, [token]);

    // Función para iniciar sesión con un token.
    const login = (token) => {
        setToken(token);
        localStorage.setItem('token', token);
    };

    // Función para cerrar sesión.
    const logout = () => {
        setToken(null);
        setUser(null);
        localStorage.removeItem('token');
    };

    // Devolvemos el proveedor de autenticación con sus propiedades.
    return (
        <AuthContext.Provider
            value={{
                token,
                user,
                setUser,
                login,
                logout
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

// Exportamos el contexto de autenticación.
export default AuthContext;