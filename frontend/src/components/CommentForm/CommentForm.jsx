import React, { useState } from 'react';
import useAuth from '../../hooks/useAuth';
import { toast } from 'react-toastify';

// Importamos CSS
import './CommentForm.css';

const apiUrl = import.meta.env.VITE_API_URL;

function CommentForm({
    recommendationId,
    recommendation,
    setRecommendation,
    onCommentSubmit
}) {
    const { token } = useAuth();
    const [comment, setComment] = useState('');
    const [vote, setVote] = useState(0);

    const handleSubmit = async (e) => {
        try {
            e.preventDefault();

            const res = await fetch(
                `${apiUrl}/recomendaciones/comentarios/${recommendationId}`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-access-token': token
                    },
                    body: JSON.stringify({
                        Comment: comment,
                        vote
                    })
                }
            );

            const body = await res.json();

            if (body.status === 'error') {
                throw new Error(body.message);
            }

            const updatedRecom = {
                ...recommendation,
                mediaVotos: body.data.mediaVotos,
                comments: [
                    ...recommendation.comments,
                    {
                        id: body.data.comment.id,
                        Comment: comment,
                        vote,
                        username: body.data.comment.username,
                        createAt: new Date()
                    }
                ]
            };

            setRecommendation(updatedRecom);

            onCommentSubmit({
                // Llamamos a la función onCommentSubmit para agregar el nuevo comentario
                id: body.data.comment.id,
                comment: comment,
                vote,
                userName: body.data.comment.username,
                createAt: new Date()
            });
            toast.success('Comentario enviado con éxito');
        } catch (err) {
            toast.error(err.message);
        } finally {
            setComment('');
            setVote(0);
        }
    };

    return (
        <form className="cform" onSubmit={handleSubmit}>
            <label htmlFor="comment">Comentario:</label>
            <textarea
                id="comment"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
            />

            <label htmlFor="vote">Voto:</label>

            <span className="star-rating">
                {[1, 2, 3, 4, 5].map((i) => (
                    <span
                        key={i}
                        className={`star ${i <= vote ? 'filled' : ''}`}
                        onClick={() => setVote(i)}
                    >
                        ★
                    </span>
                ))}
            </span>

            <button className="commentFormButton" type="submit">
                Enviar
            </button>
        </form>
    );
}

export default CommentForm;
