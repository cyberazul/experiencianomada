// Importamos los hooks.
import { useEffect, useState } from 'react';
import useAuth from '../../hooks/useAuth';

// Importamos toast.
import { toast } from 'react-toastify';

// Importamos los componentes.
import CommentForm from '../CommentForm/CommentForm';
import CommentList from '../CommentList/CommentList';

// Importamos CSS.
import './RecommendationDetail.css';

// Definimos la URL de la API.
const apiUrl = import.meta.env.VITE_API_URL;

// Definimos la función RecommendationDetail que recibe recommendationId como argumento.
function RecommendationDetail({ recommendationId }) {
    // Obtenemos el usuario autenticado utilizando el hook useAuth.
    const { user } = useAuth();

    // Definimos el estado local para recommendation y isLoading.
    const [recommendation, setRecommendation] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [comments, setComments] = useState([]);

    // Utilizamos useEffect para cargar los datos de la recomendación cuando el componente se monta.
    useEffect(() => {
        // Definimos la función fetchData para obtener los datos de la API.
        const fetchData = async () => {
            try {
                setIsLoading(true);

                // Realizamos una solicitud a la API para obtener la recomendación específica.
                const res = await fetch(
                    `${apiUrl}/recomendaciones/${recommendationId}`
                );

                // Analizamos la respuesta en formato JSON.
                const body = await res.json();

                // Verificamos si la respuesta indica un error.
                if (body.status === 'error') {
                    throw new Error(body.message);
                }

                // Establecemos la recomendación en el estado local.
                setRecommendation(body.data);
                setComments(body.data.comments);
            } catch (error) {
                // En caso de error, mostramos una notificación de error utilizando toast.
                toast.error('Error:', error.message);
            } finally {
                // Independientemente de si la solicitud tuvo éxito o no, establecemos isLoading en falso.
                setIsLoading(false);
            }
        };

        // Llamamos a la función fetchData para cargar los datos de la recomendación.
        fetchData();
    }, []); // El segundo argumento [] indica que esta función de efecto se ejecuta solo una vez al montar el componente.

    // Función para agregar un nuevo comentario a la lista de comentarios
    const addComment = (newComment) => {
        setComments([...comments, newComment]);
        console.log(newComment);
    };

    return (
        // Renderizamos el contenido solo si la recomendación está disponible.
        recommendation && (
            <div>
                {isLoading ? (
                    // Mostramos un mensaje de carga mientras isLoading es verdadero.
                    <div>Loading...</div>
                ) : (
                    <>
                        <div>
                            <div className="gridDetail">
                                <div className="boxUnoDetail">
                                    <p>
                                        {/*Renderizamos el detalle de la recomendación*/}{' '}
                                        {new Date(
                                            recommendation.createAt
                                        ).toLocaleString()}{' '}
                                    </p>
                                    <p>Creado por: {recommendation.name}</p>
                                </div>
                                <div className="boxDosDetail">
                                    {recommendation.photo && (
                                        <img
                                            src={`${apiUrl}/uploads/${recommendation.photo}`}
                                            alt="Recommendation"
                                        />
                                    )}
                                </div>
                                <p className="boxTresDetail">
                                    <span className="star-rating">
                                        {[1, 2, 3, 4, 5].map((i) => (
                                            <span
                                                key={i}
                                                className={`star ${
                                                    i <=
                                                    parseInt(
                                                        recommendation.mediaVotos
                                                    )
                                                        ? 'filled'
                                                        : ''
                                                }`}
                                            >
                                                ★
                                            </span>
                                        ))}
                                    </span>
                                </p>
                                <div className="boxCuatroDetail" />
                                <div className="boxCincoDetail" />
                                <h2 className="boxSeisDetail">
                                    {recommendation.title}
                                </h2>
                                <div className="boxSieteDetail" />
                                <div className="boxOchoDetail">
                                    <p>
                                        <span className="colorViolet">
                                            Lugar :{' '}
                                        </span>
                                        <span className="colorBlack">
                                            {recommendation.place}
                                        </span>
                                        <span className="colorViolet">
                                            {' '}
                                            / Categoría :{' '}
                                        </span>
                                        <span className="colorBlack">
                                            {recommendation.category}
                                        </span>
                                    </p>
                                </div>
                                <div className="boxNueveDetail">
                                    <p className="colorBlack">
                                        {recommendation.recommendation}
                                    </p>
                                </div>
                            </div>
                        </div>

                        {user ? (
                            <CommentForm
                                recommendationId={recommendationId}
                                recommendation={recommendation}
                                setRecommendation={setRecommendation}
                                onCommentSubmit={addComment}
                            />
                        ) : (
                            <p className="colorViolet">
                                Debes iniciar sesión para dejar un comentario.
                            </p>
                        )}

                        <CommentList comments={comments} />
                    </>
                )}
            </div>
        )
    );
}

export default RecommendationDetail;
