// Importamos los hooks.
import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';

// Importamos el context.
import AuthContext from '../context/AuthContext';

const RecomendacionesList = () => {
    const navigate = useNavigate();
    const { token, user } = useContext(AuthContext);
    const [recomendaciones, setRecomendaciones] = useState([]);
    const [recomendacionSeleccionada, setRecomendacionSeleccionada] =
        useState(null);
    const apiUrl = import.meta.env.VITE_API_URL;

    // Esta función se ejecuta cuando el componente se monta y obtiene las recomendaciones desde la API.
    useEffect(() => {
        fetch(`${apiUrl}/recomendaciones`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Error al obtener las recomendaciones');
                }
                return response.json();
            })
            .then((data) => {
                setRecomendaciones(data.data);
            })
            .catch((error) => {
                console.error('Error al obtener las recomendaciones:', error);
            });
    }, [apiUrl]);

    // Función para formatear la fecha en horario local.
    const formatLocalDate = (isoDate) => {
        const date = new Date(isoDate);
        return date.toLocaleString();
    };

    // Esta función se llama al hacer clic en un elemento de la lista para mostrar los detalles.
    const mostrarDetalles = (recomendacion) => {
        navigate(`/recom/${recomendacion.id}`);
    };

    // Esta función se llama al eliminar una recomendación.
    const borrarRecomendacion = (recomendacion) => {
        fetch(`${apiUrl}/recomendaciones/${recomendacion.id}`, {
            method: 'DELETE',
            headers: {
                'x-access-token': token
            }
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Error al borrar la recomendación');
                }
                return response.json();
            })
            .then((data) => {
                console.log(data);
                
// Actualizamos la lista de recomendaciones después de eliminar una.
                const nuevasRecomendaciones = recomendaciones.filter(
                    (rec) => rec.id !== recomendacion.id
                );
                setRecomendaciones(nuevasRecomendaciones);
            })
            .catch((error) => {
                console.error('Error al borrar la recomendación:', error);
            });
    };

    return (
        <div>
            <h2>Últimas Recomendaciones</h2>
            <div style={{ backgroundColor: '#eee' }}>
                {recomendaciones.map((recomendacion) => (
                    <div
                        key={recomendacion.id}
                        style={{
                            paddingLeft: '10px',
                            paddingRight: '40px',
                            backgroundColor: '#eee'
                        }}
                    >
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}
                        >
                            <h3>{recomendacion.title}</h3>
                            {user && recomendacion.name === user.name && (
                                <button
                                    onClick={() =>
                                        borrarRecomendacion(recomendacion)
                                    }
                                >
                                    Borrar
                                </button>
                            )}
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}
                        >
                            <p>
                                <b>{recomendacion.name}</b>
                            </p>
                            <p>
                                <span className="star-rating">
                                    {[1, 2, 3, 4, 5].map((i) => (
                                        <span
                                            key={i}
                                            className={`star ${
                                                i <=
                                                parseInt(
                                                    recomendacion.mediaVotos
                                                )
                                                    ? 'filled'
                                                    : ''
                                            }`}
                                        >
                                            ★
                                        </span>
                                    ))}
                                </span>
                            </p>
                        </div>

                        <p>
                            <b>Categoría:</b> {recomendacion.category}
                        </p>
                        <p>
                            <b>Lugar:</b> {recomendacion.place}
                        </p>

                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}
                        >
                            {recomendacion.photo && (
                                <img
                                    src={`${apiUrl}/uploads/${recomendacion.photo}`}
                                    style={{ maxWidth: '90px' }}
                                />
                            )}
                            <button
                                onClick={() => mostrarDetalles(recomendacion)}
                            >
                                Ver Detalles
                            </button>
                        </div>
                        <hr
                            style={{ marginTop: '10px', marginBottom: '10px' }}
                        />
                    </div>
                ))}
            </div>
            {recomendacionSeleccionada && (
                <div style={{ backgroundColor: 'white', padding: '15px' }}>
                    <h3>Detalle de la Recomendación</h3>
                    <p>Título: {recomendacionSeleccionada.title}</p>
                    <p>Texto: {recomendacionSeleccionada.recommendation}</p>
                    <p>
                        {' '}
                        Fecha:{' '}
                        {formatLocalDate(
                            recomendacionSeleccionada.createAt
                        )}{' '}
                    </p>
                </div>
            )}
        </div>
    );
};

export default RecomendacionesList;
