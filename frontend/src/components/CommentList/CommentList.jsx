// Importamos CSS
import './CommentList.css';

function CommentList({ comments }) {
    return (
        <div>
            <h3>Lista Comentarios:</h3>

            <ul>
                {comments.map((comment) => (
                    <li className="liCl" key={comment.id}>
                        <div className="divFlex">
                            <p className="pUno">
                                Fecha:{' '}
                                {new Date(comment.createAt).toLocaleString()}{' '}
                                {/* Muestra la fecha de creación del comentario en formato legible.*/}
                            </p>
                            <p className="pUno">{comment.userName}</p>{' '}
                            {/*  Muestra el nombre de usuario que hizo el comentario.*/}
                        </div>
                        <p className="pDos">{comment.comment}</p>{' '}
                        {/*  Muestra el contenido del comentario.*/}
                        <p>
                            <span className="star-rating">
                                {[1, 2, 3, 4, 5].map((i) => (
                                    <span
                                        key={i}
                                        className={`star ${
                                            i <= parseInt(comment.vote)
                                                ? 'filled'
                                                : ''
                                        }`}
                                    >
                                        ★
                                    </span>
                                ))}{' '}
                                {/* Muestra la calificación del comentario en
                                forma de estrellas.*/}
                            </span>
                        </p>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default CommentList;
