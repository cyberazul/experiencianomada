// Importamos los hooks.
import { useState, useEffect } from 'react';
import useAuth from '../hooks/useAuth';

// Importamos toast.
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// URL del servidor para enviar los datos
const apiUrl = import.meta.env.VITE_API_URL;

function ProfileForm() {
    // Obtenemos información de autenticación del usuario.
    const { user, setUser, token } = useAuth();

    // Definimos estados para el nombre, correo y contraseña.
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // Utilizamos useEffect para cargar los datos del usuario si ya ha iniciado sesión.
    useEffect(() => {
        if (user) {
            setName(user.name);
            setEmail(user.email);
        }
    }, [user]);

    // Función para manejar el envío del formulario.
    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            // Enviamos los datos del usuario al servidor.
            const res = await fetch(`${apiUrl}/usuario/perfil`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
                body: JSON.stringify({
                    name,
                    password,
                    email
                })
            });

            // Parseamos la respuesta del servidor.
            const body = await res.json();

            if (body.status === 'error') {
                throw new Error(body.message);
            }

            // Actualizamos los datos del usuario en el State.
            setUser({
                ...user,
                name,
                email
            });

            // Mostramos una notificación de éxito.
            toast.success(body.message);
        } catch (err) {
            // Manejamos los errores y mostramos una notificación de error.
            console.error('Error:', err);
            toast.error(err.message);
        }
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="name">Nombre:</label>
                    <input
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)} // Actualizamos el estado 'name' cuando cambia el valor del input.
                    />
                    <label htmlFor="email">Correo Electrónico:</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <label htmlFor="password">Contraseña:</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <button className="profileButton" type="submit">
                    Actualizar
                </button>
            </form>
        </>
    );
}

export default ProfileForm;
