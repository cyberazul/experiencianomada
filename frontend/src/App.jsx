// Importamos el archivo css.
import './App.css';

// Importamos router.
import { Routes, Route } from 'react-router-dom';

// Importamos toast.
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Importamos el layout.
import Header from './layout/Header/Header';
import Footer from './layout/Footer';

// Importamos las páginas.
import HomePage from './pages/HomePage/HomePage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import LoginPage from './pages/LoginPage/LoginPage';
import SingleRecomPage from './pages/SingleRecomPage/SingleRecomPage';
import ProfilePage from './pages/ProfilePage/ProfilePage';
import NewRecomPage from './pages/NewRecomPage/NewRecomPage';
import NotFound from './pages/NotFound/NotFound';
import SearchResultsPage from './pages/SearchResultsPage/SearchResultsPage';

// La función 'App' es el componente principal de la aplicación.
function App() {
    return (
        <div className="container">
            <Header />

            <main>
                {/* Agregar el ToastContainer en este caso posicion Alto-derecha y tiempo 4 segundos */}
                <ToastContainer
                    position="top-right"
                    autoClose={4000}
                    hideProgressBar={false}
                />

                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route path="/register" element={<RegisterPage />} />
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/recom/:id" element={<SingleRecomPage />} />
                    <Route path="/newrecom" element={<NewRecomPage />} />
                    <Route path="/profile" element={<ProfilePage />} />
                    <Route path="/results" element={<SearchResultsPage />} />
                    <Route path="/*" element={<NotFound />} />
                </Routes>
            </main>

            <Footer />
        </div>
    );
}
export default App;
