import { useContext } from 'react'; // Importamos el hook 'useContext' desde React.
import { NavLink } from 'react-router-dom'; // Importamos el componente 'NavLink' desde React Router.
import logo from '../../assets/ExperienciaNomadaLogo.svg'; // Importamos la imagen del logo desde la ubicación de archivos.
import AuthContext from '../../context/AuthContext'; // Importamos el contexto 'AuthContext'.
import './Header.css'; // Importamos el archivo de estilos CSS.

function Header() {
    const { token, logout } = useContext(AuthContext); // Utilizamos 'useContext' para obtener 'token' y 'logout' del contexto 'AuthContext'.

    return (
        <>
            {/*Contenedor principal del encabezado.*/}
            <div className="header-container">
                {' '}
                {/*Enlace a la página de inicio.*/}
                <NavLink to="/">
                    {' '}
                    {/* Imagen del logo con atributos.*/}
                    <img
                        src={logo}
                        width="200%"
                        alt="logo experiencia nomada"
                    />{' '}
                </NavLink>
                {!token ? ( // Comprobamos si no hay un 'token' (usuario no autenticado).
                    <div className="login-register-links">
                        {' '}
                        {/* Contenedor de enlaces de inicio de sesión y registro.*/}
                        <NavLink to="/register" className="styled-link">
                            {' '}
                            {/* Enlace para registrarse.*/}
                            Registro
                        </NavLink>
                        <NavLink to="/login" className="styled-link">
                            {' '}
                            {/* Enlace para iniciar sesión.*/}
                            Login
                        </NavLink>
                    </div>
                ) : (
                    // Si hay un 'token' (usuario autenticado).
                    <div>
                        <NavLink to="/newrecom" className="styled-link">
                            {' '}
                            {/* Enlace para agregar una nueva recomendación.*/}
                            Añadir nueva recomendación
                        </NavLink>
                        <NavLink to="/profile" className="styled-link">
                            {' '}
                            {/* Enlace para actualizar el perfil.*/}
                            Actualizar perfil
                        </NavLink>
                        <button onClick={logout} className="styled-link">
                            {' '}
                            {/* Botón para cerrar sesión.*/}
                            Logout
                        </button>
                    </div>
                )}
            </div>
        </>
    );
}

export default Header; // Exportamos el componente 'Header' para su uso en otros lugares de la aplicación.
