// Importamos los hooks
import { useRef } from 'react';

// Importamos la librería de iconos.
import { FaSearch } from 'react-icons/fa';

// Importamos el archivo css.
import './SearchBar.css';

// Definición del componente SearchBar que acepta las funciones setPlace, setCategory y loading como argumentos.
function SearchBar({ setPlace, setCategory, loading }) {
    // Creamos referencias para los campos de entrada de lugar y categoría.
    const placeInputRef = useRef();
    const categoryInputRef = useRef();

    // Manejador para el evento de envío del formulario.
    const handleSubmit = (e) => {
        e.preventDefault(); // Prevenir el envío predeterminado del formulario
        setPlace(placeInputRef.current.value); // Establece el valor del lugar con el contenido del campo de entrada de lugar.
        setCategory(categoryInputRef.current.value); // Establece el valor de la categoría con el contenido del campo de entrada de categoría.
    };

    // Renderiza el formulario de búsqueda.
    return (
        <form onSubmit={handleSubmit} className="SearchBar">
            <div>
                {/* Campo de entrada para el lugar.*/}
                <label htmlFor="destino">Buscar destino:</label>
                <input type="text" ref={placeInputRef} />{' '}
            </div>

            <div>
                {/*Campo de selección de categoría.*/}
                <label htmlFor="categoría">Categoría:</label>
                <select ref={categoryInputRef}>
                    <option value="">Selecciona categoría...</option>
                    <option value="sol y playa">Sol y playa</option>
                    <option value="rural">Rural</option>
                    <option value="gastronómico">Gastronómico</option>
                    <option value="naturaleza">Naturaleza</option>
                    <option value="cultural">Cultural</option>
                    <option value="otro">Otro</option>
                </select>{' '}
            </div>

            <button type="submit" disabled={loading} className="searchButton">
                Buscar{' '}
                {/*Botón para iniciar la búsqueda, deshabilitado si loading es verdadero.*/}
            </button>
        </form>
    );
}

export default SearchBar;
