function Footer() {
    return (
        <>
            <footer style={{ marginTop: '40px' }}>
                © 2023 Experiencia Nómada
            </footer>
        </>
    );
}

export default Footer;
