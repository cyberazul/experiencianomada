// Importamos el archivo css.
import './NotFound.css';

function NotFound() {
    return (
        <div className="notFound">
            <img src="notFound.jpg" alt="Pagina no encontrada" />
        </div>
    );
}

export default NotFound;
