// Importamos los hooks.
import { useState, useContext } from 'react';
import useAuth from '../../hooks/useAuth';
import { Navigate } from 'react-router-dom';

// Importamos toast.
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Importamos el archivo css.
import './LoginPage.css';

const apiUrl = import.meta.env.VITE_API_URL;

function LoginPage() {
    const { user, login } = useAuth();

    const [email, setEmail] = useState(''); // Cambio de 'username' a 'email'
    const [password, setPassword] = useState('');

    // Si el usuario está logeado redirigimos a la página principal.
    if (user) return <Navigate to="/" />;

    const handleLogin = async (e) => {
        e.preventDefault();

        try {
            // Realizar una solicitud POST al servidor Node.js para iniciar sesión
            const res = await fetch(`${apiUrl}/usuario/loguear`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password }) // Cambio de 'username' a 'email'
            });

            // Obtenemos el body de la respuesta.
            const body = await res.json();

            if (body.status === 'error') {
                toast.error(body.message);
            }

            // Almacenar el token en el contexto
            login(body.data.token);

            // Mostrar mensaje de éxito
            toast.success('Inicio de sesión correcto');
        } catch (error) {
            console.error('Error:', error);
            // Mostrar mensaje de error
            toast.error('Error!! El inicio de sesión no tuvo exito');
        } finally {
            // Borramos el contenido de los inputs.
            setEmail('');
            setPassword('');
        }
    };

    return (
        <div className="formLoginDiv">
            <form onSubmit={handleLogin} className="formLogin">
                <input
                    type="text"
                    placeholder="Email" // Cambio de 'Nombre de usuario' a 'Email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)} // Cambio de 'username' a 'email'
                />
                <input
                    type="password"
                    placeholder="Contraseña"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <button className="loginButton" type="submit">
                    Iniciar sesión
                </button>
            </form>
        </div>
    );
}

export default LoginPage;
