// Importamos los hooks.
import { useState } from 'react';
import useAuth from '../../hooks/useAuth';
import { Navigate, useNavigate } from 'react-router-dom';

// Importamos toast.
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const apiUrl = import.meta.env.VITE_API_URL;

// Importamos el archivo css.
import './NewRecomPage.css';

function NewRecomPage() {
    // Obtenemos el usuario y el token de autenticación.
    const { user, token } = useAuth();
    const navigate = useNavigate();

    // Definimos el estado inicial del formulario.
    const [formData, setFormData] = useState({
        title: '',
        image: null,
        place: '',
        category: '',
        text: ''
    });
    const [isSubmitting, setIsSubmitting] = useState(false);

    // Si el usuario NO está logeado redirigimos a la página principal.
    if (!user) return <Navigate to="/" />;

    // Manejador de envío del formulario.
    const handleSubmit = async (event) => {
        event.preventDefault();

        if (!token) {
            navigate('/login');
            return;
        }

        // Establecemos el estado de envío a verdadero.
        setIsSubmitting(true);

        // Creamos un objeto FormData con los datos del formulario.
        const data = new FormData();

        data.append('title', formData.title);
        data.append('place', formData.place);
        data.append('category', formData.category);
        data.append('text', formData.text);

        // Si se proporcionó una imagen, la agregamos a los datos del formulario.
        if (formData.image) {
            data.append('image', formData.image);
        }

        try {
            // Realizamos una solicitud POST a la API con los datos del formulario.
            const response = await fetch(`${apiUrl}/recomendaciones`, {
                method: 'POST',
                body: data,
                headers: {
                    'x-access-token': token
                }
            });

            if (response.ok) {
                // Si la respuesta es exitosa, mostramos un mensaje de éxito y redirigimos al usuario a la página principal.
                const responseData = await response.json();

                toast.success(responseData.message);
                navigate('/');
            } else {
                // Si la respuesta no es exitosa, mostramos un mensaje de error y restablecemos el estado de envío.
                const errorData = await response.json();
                toast.error(errorData.message);
                setIsSubmitting(false);
            }
        } catch (error) {
            // Manejamos cualquier error que ocurra durante la solicitud.
            console.error('Error:', error);
            toast.error('Hubo un error al procesar la solicitud.');
        }
    };
    return (
        <div className="newRecomDiv">
            <h2>Formulario nueva recomendación:</h2>
            <form onSubmit={handleSubmit} className="newRecomForm">
                <div>
                    <label htmlFor="title">Insertar título:</label>
                    <input
                        type="text"
                        id="title"
                        name="title"
                        value={formData.title}
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                title: e.target.value
                            })
                        }
                        required
                        alt="Título de la recomendación"
                    />
                </div>
                <div>
                    <label htmlFor="image">Insertar imagen</label>
                    <input
                        type="file"
                        id="image"
                        name="image"
                        accept="image/*"
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                image: e.target.files[0]
                            })
                        }
                        alt="Imagen de la recomendación"
                    />
                    {formData.image ? (
                        <figure>
                            <img
                                src={URL.createObjectURL(formData.image)}
                                style={{ width: '100px' }}
                                alt="Vista previa de la imagen"
                            />
                        </figure>
                    ) : null}
                </div>
                <div>
                    <label htmlFor="place">Insertar destino: </label>
                    <input
                        type="text"
                        id="place"
                        name="place"
                        value={formData.place}
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                place: e.target.value
                            })
                        }
                        required
                        alt="Destino de la recomendación"
                    />
                </div>
                <div>
                    <label htmlFor="category">Insertar categoría: </label>
                    <select
                        name="category"
                        id="category"
                        value={formData.category}
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                category: e.target.value
                            })
                        }
                        required
                        alt="Categoría de la recomendación"
                    >
                        <option value="">Selecciona categoría</option>
                        <option value="sol y playa">Sol y playa</option>
                        <option value="rural">Rural</option>
                        <option value="gastronómico">Gastronómico</option>
                        <option value="naturaleza">Naturaleza</option>
                        <option value="cultural">Cultural</option>
                        <option value="otro">Otro</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="text">Insertar texto recomendación: </label>
                    <textarea
                        id="text"
                        name="text"
                        value={formData.text}
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                text: e.target.value
                            })
                        }
                        required
                        alt="Texto de la recomendación"
                    ></textarea>
                </div>
                <button
                    className="newRecomButton"
                    type="submit"
                    disabled={isSubmitting}
                >
                    Insertar
                </button>
            </form>
        </div>
    );
}

export default NewRecomPage;
