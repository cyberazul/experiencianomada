// Importamos los hooks.
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

// Importamos la librería de iconos.
import { BsArrowDownUp } from 'react-icons/bs';

// Importamos los componentes.
import SearchBar from '../../layout/SearchBar/SearchBar';

// Importamos el fichero css.
import './SearchResultsPage.css';

// Importamos la URL de nuestra API.
const apiUrl = import.meta.env.VITE_API_URL;

function SearchResultsPage() {
    const navigate = useNavigate();

    const [recommedations, setRecommendations] = useState([]);
    const [place, setPlace] = useState('');
    const [category, setCategory] = useState('');
    const [sortBy, setSortBy] = useState('desc');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        // Función que busca recomendaciones.
        const fetchRecommendations = async () => {
            try {
                setLoading(true);

                const res = await fetch(
                    `${apiUrl}/recomendaciones?place=${place}&category=${category}`
                );

                const body = await res.json();

                if (body.status === 'error') {
                    throw new Error(body.message);
                }

                // Establecemos las recomendaciones.
                setRecommendations(body.data);
            } catch (err) {
                // Gestionar el error.
                console.error(err);
            } finally {
                setLoading(false);
            }
        };
    }, [place, category]);

    // Ir a la página de detalles de recomendación.
    const mostrarDetalles = (result) => {
        navigate(`/recom/${result.id}`);
    };

    // Función para ordenar los resultados por fecha
    const sortResultsByDate = () => {
        const sortedResults = [...recommedations];

        sortedResults.sort((a, b) => {
            const dateA = new Date(a.createAt);
            const dateB = new Date(b.createAt);

            if (sortBy === 'asc') {
                return dateA - dateB;
            } else {
                return dateB - dateA;
            }
        });

        // Establecemos el array de recomendaciones ordenado por fecha.
        setRecommendations(sortedResults);
    };

    // Función para cambiar el orden de los resultados
    const toggleSortOrder = () => {
        setSortBy(sortBy === 'asc' ? 'desc' : 'asc');
    };

    if (loading) {
        return <p>Cargando resultados...</p>;
    }

    if (recommedations.length === 0) {
        return (
            <p>
                No hemos encontrado ninguna coincidencia en nuestra base de
                datos para la solicitud realizada.
            </p>
        );
    }

    return (
        <div>
            <SearchBar
                place={place}
                setPlace={setPlace}
                category={category}
                setCategory={setCategory}
                loading={loading}
            />
            <h2>Resultados de búsqueda:</h2>
            <div>
                <button onClick={toggleSortOrder}>
                    <BsArrowDownUp />
                </button>
            </div>
            <ul>
                {recommedations.map((result, index) => (
                    <li key={result.id}>
                        {result.photo && (
                            <img
                                src={`${apiUrl}/uploads/${result.photo}`}
                                alt="Recommendation"
                            />
                        )}
                        <h3>{result.title}</h3>
                        <p>Viaje:{result.recommendation}</p>
                        <p>Categoría: {result.category}</p>
                        <p>Lugar: {result.place}</p>
                        <p>Número de Likes: {result.votes}</p>
                        <p>Nombre del Creador: {result.name}</p>
                        <p> {new Date(result.createAt).toLocaleString()} </p>
                        <button onClick={() => mostrarDetalles(result)}>
                            Ver Detalles
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default SearchResultsPage;
