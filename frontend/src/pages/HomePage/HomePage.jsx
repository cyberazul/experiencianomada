// Importamos los hooks.
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';

// Importamos la librería de iconos.
import { BsArrowDownUp } from 'react-icons/bs';
import { AiOutlineHome } from 'react-icons/ai';

// Importamos los componentes.
import SearchBar from '../../layout/SearchBar/SearchBar';

// Importamos la URL de nuestra API.
const apiUrl = import.meta.env.VITE_API_URL;

// Importamos el archivo css.
import './HomePage.css';

function HomePage() {
    const navigate = useNavigate();
    const { user, token } = useAuth();

    const [recommedations, setRecommendations] = useState([]);
    const [place, setPlace] = useState('');
    const [category, setCategory] = useState('');
    const [sortBy, setSortBy] = useState('desc');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        // Función que busca recomendaciones.
        const fetchRecommendations = async () => {
            try {
                setLoading(true);

                const res = await fetch(
                    `${apiUrl}/recomendaciones?place=${place}&category=${category}`
                );

                const body = await res.json();

                if (body.status === 'error') {
                    throw new Error(body.message);
                }

                // Establecemos las recomendaciones.
                setRecommendations(body.data);
            } catch (err) {
                // Gestionar el error.
                console.error(err);
            } finally {
                setLoading(false);
            }
        };

        // Ejecutamos la función anterior.
        fetchRecommendations();
    }, [place, category]);

    // Ir a la página de detalles de recomendación.
    const mostrarDetalles = (recommedation) => {
        navigate(`/recom/${recommedation.id}`);
    };

    // Funcion para Borrar una recomendacion
    const borrarRecomendacion = (recommedation) => {
        fetch(`${apiUrl}/recomendaciones/${recommedation.id}`, {
            method: 'DELETE',
            headers: {
                'x-access-token': token
            }
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Error al borrar la recomendación');
                }
                return response.json();
            })
            .then((data) => {
                console.log(data);
                // Filtramos las recomendaciones para quitar la que fue eliminada
                const nuevasRecomendaciones = recommedations.filter(
                    (rec) => rec.id !== recommedation.id
                );
                setRecommendations(nuevasRecomendaciones);
            })
            .catch((error) => {
                console.error('Error al borrar la recomendación:', error);
            });
    };

    // Función para ordenar los resultados por fecha
    const sortResultsByDate = () => {
        const sortedResults = [...recommedations];

        sortedResults.sort((a, b) => {
            const dateA = new Date(a.createAt);
            const dateB = new Date(b.createAt);

            if (sortBy === 'asc') {
                return dateA - dateB;
            } else {
                return dateB - dateA;
            }
        });

        // Establecemos el array de recomendaciones ordenado por fecha.
        setRecommendations(sortedResults);
    };

    //Función para recargar listado general
    const reloadPage = () => {
        window.location.reload();
    };

    // Función para cambiar el orden de los resultados
    const toggleSortOrder = () => {
        setSortBy(sortBy === 'asc' ? 'desc' : 'asc');
    };

    if (loading) {
        return <p>Cargando resultados...</p>;
    }

    return (
        <section>
            <div>
                <SearchBar
                    setPlace={setPlace}
                    setCategory={setCategory}
                    loading={loading}
                />

                {recommedations.length === 0 ? (
                    <>
                        <button
                            className="home-button"
                            onClick={() => {
                                reloadPage();
                            }}
                        >
                            <AiOutlineHome />
                        </button>
                        <h2>Resultados de búsqueda:</h2>
                        <p>
                            No hemos encontrado ninguna coincidencia en nuestra
                            base de datos para la solicitud realizada.
                        </p>
                    </>
                ) : (
                    <>
                        <button
                            className="home-button"
                            onClick={() => {
                                reloadPage();
                            }}
                        >
                            <AiOutlineHome />
                        </button>
                        <button
                            className="home-button"
                            onClick={() => {
                                toggleSortOrder();
                                sortResultsByDate();
                            }}
                        >
                            <BsArrowDownUp />
                        </button>

                        <ul className="recom-list">
                            {recommedations.map((recommedation) => (
                                <li
                                    key={recommedation.id}
                                    className="recom-item"
                                >
                                    <header>
                                        <h3 className="titulo">
                                            {recommedation.title}
                                        </h3>
                                    </header>
                                    <div>
                                        <div>
                                            <p className="categoria">
                                                <strong>Categoría:</strong>{' '}
                                                {recommedation.category}
                                            </p>
                                            <p className="lugar">
                                                <strong>Lugar:</strong>{' '}
                                                {recommedation.place}
                                            </p>
                                        </div>
                                    </div>

                                    <figure>
                                        {recommedation.photo && (
                                            <img
                                                src={`${apiUrl}/uploads/${recommedation.photo}`}
                                                className="photo"
                                            />
                                        )}
                                    </figure>

                                    <footer>
                                        <div className="nombre-rating-container">
                                            <p className="nombre">
                                                <strong>
                                                    {recommedation.name}
                                                </strong>
                                            </p>
                                            <p>
                                                <span className="star-rating">
                                                    {[1, 2, 3, 4, 5].map(
                                                        (i) => (
                                                            <span
                                                                key={i}
                                                                className={`star ${
                                                                    i <=
                                                                    parseInt(
                                                                        recommedation.mediaVotos
                                                                    )
                                                                        ? 'filled'
                                                                        : ''
                                                                }`}
                                                            >
                                                                ★
                                                            </span>
                                                        )
                                                    )}
                                                </span>
                                            </p>
                                        </div>

                                        <div>
                                            {user &&
                                                recommedation.name ===
                                                    user.name && (
                                                    <button
                                                        onClick={() =>
                                                            borrarRecomendacion(
                                                                recommedation
                                                            )
                                                        }
                                                        className="delete-button"
                                                    >
                                                        Borrar
                                                    </button>
                                                )}
                                            <button
                                                onClick={() =>
                                                    mostrarDetalles(
                                                        recommedation
                                                    )
                                                }
                                                className="details-button"
                                            >
                                                Ver Detalles
                                            </button>
                                        </div>
                                    </footer>
                                </li>
                            ))}
                        </ul>
                    </>
                )}
            </div>
        </section>
    );
}

export default HomePage;
