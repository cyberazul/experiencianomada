// Importamos los hooks.
import { useState, useRef } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
// Importamos toast.
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const apiUrl = import.meta.env.VITE_API_URL;

// Importamos el archivo css.
import './RegisterPage.css';

function RegisterPage() {
    const navigate = useNavigate();
    const passwordRef = useRef(null);
    const { user } = useAuth();

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: ''
    });

    const [confirmPassword, setConfirmPassword] = useState('');

    // Si el usuario está logeado redirigimos a la página principal.
    if (user) return <Navigate to="/" />;

    // Función para manejar cambios en los campos del formulario.
    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'password') {
            setFormData({
                ...formData,
                [name]: value
            });
        } else if (name === 'confirmPassword') {
            setConfirmPassword(value);
        } else {
            setFormData({
                ...formData,
                [name]: value
            });
        }
    };

    // Función para manejar el envío del formulario.
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (formData.password === confirmPassword) {
            try {
                // Envía una solicitud POST al servidor para registrar al usuario.
                const response = await fetch(`${apiUrl}/usuario/registro`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(formData)
                });

                if (response.ok) {
                    // Si la solicitud fue exitosa, muestra un mensaje de éxito y redirige a la página de inicio de sesión.
                    const data = await response.json();
                    toast.success(data.message);
                    navigate('/login');
                } else {
                    // Muestra errores del backend en caso de una respuesta no exitosa.
                    // Aquí puedes agregar código para manejar los errores específicos del servidor.
                    const errorData = await response.json();
                    toast.error(errorData.message);
                }
            } catch (error) {
                console.error('Error:', error);
                // Muestra mensaje de error si hay problemas con el backend
                toast.error('Hubo un error al procesar la solicitud.');
            }
        } else {
            // Mostrar mensaje de error si las contraseñas no coinciden
            toast.error('Las contraseñas no coinciden');
        }
    };

    return (
        <div className="registerDiv">
            <form onSubmit={handleSubmit} className="registerForm">
                <fieldset>
                    <label>Nombre:</label>
                    <input
                        type="text"
                        name="name"
                        value={formData.name}
                        onChange={handleChange}
                    />
                </fieldset>
                <fieldset>
                    <label>Email:</label>
                    <input
                        type="email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                    />
                </fieldset>
                <fieldset>
                    <label>Contraseña</label>
                    <input
                        type="password"
                        name="password"
                        value={formData.password}
                        onChange={handleChange}
                    />
                    <p className="registerParagraph">
                        Minimo 8 caracteres, debe incluir mayuscula, minuscula,
                        número y caracter especial.
                    </p>
                </fieldset>
                <fieldset>
                    <label>Confirmar Contraseña:</label>
                    <input
                        type="password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={handleChange}
                        ref={passwordRef}
                    />
                </fieldset>
                <button className="registerButton" type="submit">
                    Registrarse
                </button>
            </form>
        </div>
    );
}

export default RegisterPage;
