// Importamos los hooks.
import { useParams } from 'react-router-dom';

// Importamos los componentes.
import RecommendationDetail from '../../components/RecommendationDetail/RecommendationDetail';

// Importamos el archivo css.
import './SingleRecomPage.css';

function SingleRecomPage() {
    // Obtenemos el id de la recomendación a la que queremos acceder.
    const { id } = useParams();

    return (
        <div>
            <RecommendationDetail recommendationId={id} />
        </div>
    );
}

export default SingleRecomPage;
