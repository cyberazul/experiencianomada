// Importamos los hooks.
import useAuth from '../../hooks/useAuth.js';
import { Navigate } from 'react-router-dom';

// Importamos los componentes.
import ProfileForm from '../../components/ProfileForm.jsx';

// Importamos el archivo css.
import './ProfilePage.css';

function ProfilePage() {
    const { user } = useAuth();

    // Si el usuario NO está logeado redirigimos a la página principal.
    if (!user) return <Navigate to="/" />;

    return (
        <section className="ProfilePageSection">
            <h1>Editar Perfil</h1>
            <ProfileForm className="ProfilePageForm" />
        </section>
    );
}

export default ProfilePage;
