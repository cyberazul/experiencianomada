// Importamos la función que crea un contexto.
import { useContext } from 'react';

// Importamos el contexto.
import AuthContext from '../context/AuthContext';

export const useAuth = () => {
    return useContext(AuthContext);
};

export default useAuth;
