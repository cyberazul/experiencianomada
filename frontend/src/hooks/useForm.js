// Importamos React.
import React from 'react';

// Definimos una función llamada useForm que recibe un objeto de inputs iniciales.
function useForm(initialInputs = {}) {
    // Usamos el hook useState de React para gestionar el estado de formValues.
    const [formValues, setFormValues] = React.useState(initialInputs);

    // Definimos una función llamada handleFormChange que se ejecutará cuando cambie el valor de un campo del formulario.
    function handleFormChange(event) {
        // Verificamos si el tipo de elemento de formulario es una casilla de verificación (checkbox).
        const newValue =
            event.target.type === 'checkbox'
                ? event.target.checked
                : event.target.value;

        // Actualizamos el estado de formValues con el nuevo valor del campo que ha cambiado.
        setFormValues({
            ...formValues, // Mantenemos los valores anteriores del formulario.
            [event.target.name]: newValue // Actualizamos el campo específico que ha cambiado.
        });
    }

    // La función useForm devuelve un array que contiene el estado actual del formulario (formValues) y la función para manejar los cambios (handleFormChange).
    return [formValues, handleFormChange];
}

// Exportamos la función useForm para que pueda ser utilizada en otros módulos.
export default useForm;
