-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ExperienciaNomada
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ExperienciaNomada
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ExperienciaNomada` DEFAULT CHARACTER SET utf8 ;
USE `ExperienciaNomada` ;

-- -----------------------------------------------------
-- Table `ExperienciaNomada`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ExperienciaNomada`.`Users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `createAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `NombreUsuario_UNIQUE` (`name` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `password_UNIQUE` (`password` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExperienciaNomada`.`recommendation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ExperienciaNomada`.`recommendation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Users_id` INT NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `place` VARCHAR(100) NOT NULL,
  `category` VARCHAR(100) NOT NULL,
  `recommendation` VARCHAR(500) NOT NULL,
  `photo` VARCHAR(100) NULL,
  `createAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_recommendation_Users_idx` (`Users_id` ASC) VISIBLE,
  CONSTRAINT `fk_recommendation_Users`
    FOREIGN KEY (`Users_id`)
    REFERENCES `ExperienciaNomada`.`Users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExperienciaNomada`.`Comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ExperienciaNomada`.`Comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Users_id` INT NOT NULL,
  `recommendation_id` INT NOT NULL,
  `Comment` VARCHAR(250) NOT NULL,
  `createAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vote` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Title_UNIQUE` (`Comment` ASC) VISIBLE,
  INDEX `fk_Comment_Users1_idx` (`Users_id` ASC) VISIBLE,
  INDEX `fk_Comment_recommendation1_idx` (`recommendation_id` ASC) VISIBLE,
  CONSTRAINT `fk_Comment_Users1`
    FOREIGN KEY (`Users_id`)
    REFERENCES `ExperienciaNomada`.`Users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Comment_recommendation1`
    FOREIGN KEY (`recommendation_id`)
    REFERENCES `ExperienciaNomada`.`recommendation` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
