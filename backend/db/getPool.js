//Requerimos el modulo de node mysql2 para gestionar las conexiones con la base de datos
const mysql = require('mysql2/promise');

//Requerimos el modulo de node dotenv para poder leer la informacion del archivo .env
require('dotenv').config();

// Creamos la configuración para conectarnos a la base de datos
const dbConfig = {
    host: process.env.MYSQL_HOST,
    port: '3306',
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB_NAME,
    timezone: 'local'
};

//Creamos la variable donde almacenaremos la piscina de conexiones
let pool;

//Creamos la funcion que gestionara la piscina de conexiones
const getPool = async () => {
    // Si la piscina no existe la creamos
    if (!pool) {
        pool = mysql.createPool(dbConfig);
    }

    //Retornamos la piscina de conexiones
    return pool;
};

module.exports = getPool;
