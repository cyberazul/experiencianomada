// Importamos las dependencias.

const jwt = require('jsonwebtoken');

// Importamos la función que permite crear errores.
const createError = require('../utils/createError');

// Middleware para autenticar a un usuario mediante un token JWT
const authUserController = async (req, res, next) => {
    try {
        // Obtener el token JWT del encabezado 'x-access-token'
        const { 'x-access-token': token } = req.headers;

        // Si no se proporciona un token, lanzar un error de no autenticado
        if (!token) {
            createError(401, 'No autenticado.');
        }

        let infoUser;

        try {
            // Verificar y decodificar el token JWT utilizando la clave secreta definida en el entorno
            infoUser = jwt.verify(token, process.env.JWT_SECRET);
        } catch {
            createError(401, 'Token no válido.');
        }

        // Asignar el ID del usuario extraído del token JWT a la solicitud
        req.userId = infoUser.userId;

        next();
    } catch (err) {
        next(err);
    }
};

module.exports = authUserController;
