const fs = require('fs/promises');

// Función para crear un directorio si no existe en el sistema de archivos
const createPathIfNotExists = async (path) => {
    try {
        await fs.access(path);
    } catch {
        await fs.mkdir(path);
    }
};

module.exports = createPathIfNotExists;
