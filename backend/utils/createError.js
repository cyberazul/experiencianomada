// Función para crear un error personalizado con un estado y mensaje
const createError = (status, message) => {
    const error = new Error(message);
    error.httpStatus = status;
    throw error;
};

module.exports = createError;
