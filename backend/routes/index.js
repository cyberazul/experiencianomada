const express = require('express');
const router = express.Router();

// Importamos las rutas de los usuarios y recomendaciones.
const userRoutes = require('./userRoutes');
const recommendationRoutes = require('./recommendationRoutes');

// Establecemos las rutas mediante un middleware.
router.use(userRoutes);
router.use(recommendationRoutes);

module.exports = router;
