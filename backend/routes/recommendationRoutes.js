const express = require('express');
const router = express.Router();

// Importamos las funciones controladoras intermedias.
const authUserController = require('../middlewares/authUserController');

// Importamos el controlador de recomendaciones.
const {
    recommendationListController,
    addCommentController,
    deleteRecommendationController,
    listCommentController,
    addRecomController,
    detailRecommendationController,
    getAscendientesRecommendations,
    getDescendientesRecommendations
} = require('../controllers/recommendations');

// Creación de nueva recomendación
router.post('/recomendaciones', authUserController, addRecomController);

// Obtener recomendaciones.
router.get('/recomendaciones', recommendationListController);

// Creación de un comentario
router.post(
    '/recomendaciones/comentarios/:id',
    authUserController,
    addCommentController
);
// Eliminar una recomendación
router.delete(
    '/recomendaciones/:id',
    authUserController,
    deleteRecommendationController
);

// Ruta para obtener recomendaciones ascendentes
router.get('/recomendaciones/ascendientes', getAscendientesRecommendations);

// Ruta para obtener recomendaciones descendentes
router.get('/recomendaciones/descendientes', getDescendientesRecommendations);
// Listado de comentarios de una recomendacion

router.get('/recomendaciones/comentarios/:id', listCommentController);

//  Detalles de Rec.
router.get('/recomendaciones/:id', detailRecommendationController);

module.exports = router;
