const express = require('express');
const router = express.Router();

// Importamos las funciones controladoras intermedias.
const authUserController = require('../middlewares/authUserController');

// Importamos las funciones controladoras finales.
const {
    newUserController,
    updateProfileController,
    loginUserController,
    getOwnUserController
} = require('../controllers/users');

// Insertar un nuevo usuario.
router.post('/usuario/registro', newUserController);

// Actualización de Perfil
router.post('/usuario/perfil', authUserController, updateProfileController);

// Ruta para iniciar sesión
router.post('/usuario/loguear', loginUserController);

// Retornamos info del usuario del token.
router.get('/usuario', authUserController, getOwnUserController);

module.exports = router;
