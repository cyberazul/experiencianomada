require('dotenv').config();
const express = require('express');
const app = express();
const fileUpload = require('express-fileupload');
const cors = require('cors');
const morgan = require('morgan');

// Importamos las rutas.
const routes = require('./routes');

const port = process.env.PORT || 3000;

app.use(cors());

app.use('/uploads', express.static('uploads'));

//leer bodies tipo form y binario
app.use(fileUpload());

app.use(express.json());

app.use(morgan('dev'));

// Indicamos a Express dónde están las rutas.
app.use(routes);

// Middleware de ruta no encontrada.
app.use((req, res) => {
    res.status(404).send({
        status: 'error',
        message: 'Página no encontrada.'
    });
});

// Middleware de gestión de errores:
app.use((err, req, res, next) => {
    console.error(err);

    res.status(err.httpStatus || 500).send({
        status: 'error',
        message: err.message
    });
});

app.listen(port, () => {
    console.log(`Servidor Express escuchando en el puerto ${port}`);
});
