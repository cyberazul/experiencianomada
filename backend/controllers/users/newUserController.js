// Importamos los módulos.
const bcrypt = require('bcrypt');

// Importamos la función que me permite conectarme con la base de datos.
const getPool = require('../../db/getPool');

// Importamos la función que permite crear errores.
const createError = require('../../utils/createError');

// Importamos el esquema.
const newUserSchema = require('../../schemas/users/newUserSchema');

// Importamos la función que valida esquemas.
const validateSchema = require('../../utils/validateSchema');

// Función controladora final que inserta un usuario.
const newUserController = async (req, res, next) => {
    try {
        await validateSchema(newUserSchema, req.body);

        // Obtenemos los datos del body.
        const { name, email, password } = req.body;

        // Comprobamos si ya existe un usuario con ese email en la base de datos.
        const pool = await getPool();
        let [users] = await pool.query('SELECT id FROM users WHERE email = ?', [
            email
        ]);

        if (users.length > 0) {
            return createError(
                res,
                409,
                'Ya existe un usuario con ese email en la base de datos'
            );
        }

        // Comprobamos si ya existe un usuario con ese nombre en la base de datos.
        [users] = await pool.query('SELECT id FROM users WHERE name = ?', [
            name
        ]);

        if (users.length > 0) {
            return createError(
                res,
                409,
                'Ya existe un usuario con ese nombre en la base de datos'
            );
        }

        // Encriptamos la contraseña.
        const hashedPassword = await bcrypt.hash(password, 10);

        // Insertamos el usuario en la base de datos.
        await pool.query(
            'INSERT INTO users (name, email, password) VALUES (?, ?, ?)',
            [name, email, hashedPassword]
        );

        res.status(201).send({
            status: 'ok',
            message: 'Usuario creado correctamente'
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newUserController;
