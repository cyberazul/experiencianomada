// Importamos los módulos.
const bcrypt = require('bcrypt');

// Importamos la función que me permite conectarme con la base de datos.
const getPool = require('../../db/getPool');

// Importamos el esquema.
const updateProfileSchema = require('../../schemas/users/updateProfileSchema');

// Importamos la función que valida esquemas.
const validateSchema = require('../../utils/validateSchema');

const updateProfileController = async (req, res, next) => {
    try {
        // Validamos los datos que envía el usuario con Joi.
        await validateSchema(updateProfileSchema, req.body);

        // Obtenemos los datos del body.
        const { name, email, password } = req.body;

        // ID del usuario que desea actualizar el perfil
        const userId = req.userId;

        let connection = await getPool();

        // Obtenemos los datos actuales del usuario.
        const [users] = await connection.query(
            `SELECT name, email FROM users WHERE id = ?`,
            [req.userId]
        );

        // Si el nombre de usuario recibido es distinto del nombre que figura en la base de datos
        // actualizamos le nombre del usuario.
        if (users[0].name !== name) {
            // Actualiza el nombre del usuario en la base de datos.
            await connection.query(
                `
                    UPDATE users 
                    SET name = ?
                    WHERE id = ?
                `,
                [name, userId]
            );
        }

        // Si el email recibido es distinto del email que figura en la base de datos
        // vamos a insertar el nuevo email en la base de datos.
        if (users[0].email !== email) {
            // Actualiza el nombre del usuario en la base de datos.
            await connection.query(
                `
                    UPDATE users 
                    SET email = ?
                    WHERE id = ?
                `,
                [email, userId]
            );
        }

        // Si recibimos una nueva contraseña la actualizamos.
        if (password) {
            const hashedPassword = await bcrypt.hash(password, 10);

            // Actualiza el nombre del usuario en la base de datos.
            await connection.query(
                `
                    UPDATE users 
                    SET password = ?
                    WHERE id = ?
                `,
                [hashedPassword, userId]
            );
        }

        res.status(200).json({
            status: 'ok',
            message: 'Perfil actualizado correctamente.'
        });
    } catch (err) {
        next(err);
    }
};

module.exports = updateProfileController;
