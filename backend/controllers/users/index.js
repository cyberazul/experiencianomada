const newUserController = require('./newUserController');
const updateProfileController = require('./updateProfileController');
const loginUserController = require('./loginUserController');
const getOwnUserController = require('./getOwnUserController');

module.exports = {
    newUserController,
    updateProfileController,
    loginUserController,
    getOwnUserController
};
