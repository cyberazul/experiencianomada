// Importamos la funcón que me permite conectarme con la base de datos.
const getPool = require('../../db/getPool');

// Importamos la función que genera errores.
const createError = require('../../utils/createError');

// FUnción controladora final que retorna ls datos del usuario del token.
const getOwnUserController = async (req, res, next) => {
    try {
        const pool = await getPool();

        // Obtenemos los datos del usuario de la BBDD.
        const [users] = await pool.query(
            `SELECT id, name, email FROM users WHERE id = ?`,
            [req.userId]
        );

        // Si no existe el usuario lanzamos un error.
        if (users.length < 0) {
            createError(404, 'Usuario no encontrado.');
        }

        // Devuelve una respuesta al cliente.
        res.json({
            status: 'ok',
            data: {
                user: users[0]
            }
        });
    } catch (error) {
        next(error);
    }
};
module.exports = getOwnUserController;
