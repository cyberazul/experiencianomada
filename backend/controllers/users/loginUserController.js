// Importamos las dependencias.
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Importamos el esquema.
const loginUserSchema = require('../../schemas/users/loginUserSchema');

// Importamos la función que valida esquemas.
const validateSchema = require('../../utils/validateSchema');

// Importamos la función que genera errores.
const createError = require('../../utils/createError');

const loginUserController = async (req, res, next) => {
    try {
        // Validamos los datos que envía el usuario con Joi.
        await validateSchema(loginUserSchema, req.body);

        const { email, password } = req.body;

        // Comprobar si el email existe en la BBDD
        const pool = await getPool();

        // Consultar la base de datos para encontrar un usuario con el email proporcionado
        const [[user]] = await pool.query(
            `
                SELECT * 
                FROM users
                WHERE email = ?
            `,
            [email]
        );

        if (!user) {
            createError(401, 'El usuario y/o contraseña son incorrectos.');
        }

        // Comparar la contraseña con la de la BBDD
        const passwordMatch = await bcrypt.compare(password, user.password);

        if (!passwordMatch) {
            createError(401, 'El usuario y/o contraseña son incorrectos.');
        }

        // Objeto con los datos que queremos almacenar en el token.
        const tokenInfo = {
            userId: user.id
        };

        // Si el usuario y la contraseña son correctos, genera un token JWT
        const token = jwt.sign(tokenInfo, process.env.JWT_SECRET, {
            expiresIn: '30d'
        });

        // Devuelve el token en la respuesta.
        res.json({
            status: 'ok',
            data: {
                token
            }
        });
    } catch (error) {
        next(error);
    }
};
module.exports = loginUserController;
