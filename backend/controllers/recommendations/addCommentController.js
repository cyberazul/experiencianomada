// Importamos la función que me permite conectarme con la base de datos.
const getPool = require('../../db/getPool');

// Importamos el esquema.
const newCommentSchema = require('../../schemas/recommendations/newCommentSchema');
const createError = require('../../utils/createError');

// Importamos la función que valida esquemas.
const validateSchema = require('../../utils/validateSchema');

// Controlador para agregar un nuevo comentario.
const addCommentController = async (req, res, next) => {
    const recomId = req.params.id;
    const userId = req.userId;

    try {
        await validateSchema(newCommentSchema, req.body);

        const { Comment, vote } = req.body;

        const pool = await getPool();

        // Comprobamos si el usuario ya ha comentado esta recomendación.
        const [comments] = await pool.query(
            `
                SELECT id FROM comment WHERE Users_id = ? AND recommendation_id = ?
            `,
            [userId, recomId]
        );

        // Si el usuario ya ha comentado lanzamos un error.
        if (comments.length > 0) {
            createError(
                409,
                'Ya existe un comentario realizado por ese usuario'
            );
        }

        // Insertamos el comentario.
        const [newComment] = await pool.query(
            `
                INSERT INTO comment (Users_id, recommendation_id, Comment, vote)
                VALUES (?, ?, ?, ?)
            `,
            [userId, recomId, Comment, vote]
        );

        // Obtenemos la nueva media de votos de la recomendación.
        const [recommendations] = await pool.query(
            `
                SELECT 
                    u.name,
                    AVG(IFNULL(c.vote, 0)) AS mediaVotos
                FROM recommendation AS r 
                LEFT JOIN comment AS c ON (r.id = c.recommendation_id)
                INNER JOIN Users u ON (u.id = r.Users_id)
                WHERE r.id = ?
            `,
            [recomId]
        );

        res.status(200).send({
            status: 'ok',
            data: {
                mediaVotos: recommendations[0].mediaVotos,
                comment: {
                    id: newComment.insertId,
                    username: recommendations[0].name
                }
            },
            message: 'Comentario creado correctamente.'
        });
    } catch (err) {
        next(err);
    }
};

module.exports = addCommentController;
