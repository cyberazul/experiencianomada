// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Controlador para eliminar una recomendación
const deleteRecommendationController = async (req, res) => {
    // Obtener el ID de la recomendación a eliminar desde los parámetros de la solicitud
    const recomendacionId = req.params.id;

    // Obtener una conexión a la base de datos
    const pool = await getPool();

    try {
        // Eliminar la recomendación de la base de datos
        const [result] = await pool.query(
            'DELETE FROM recommendation WHERE id = ?',
            [recomendacionId]
        );

        // Comprobar si se eliminó con éxito
        if (result.affectedRows === 0) {
            return res
                .status(404)
                .json({ message: 'Recomendación no encontrada' });
        }

        res.status(200).json({
            message: 'Se eliminó la recomendación con éxito'
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error al eliminar la recomendación' });
    }
};

module.exports = deleteRecommendationController;
