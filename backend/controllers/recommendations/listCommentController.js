// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Controlador para listar los comentarios de una recomendación
const listCommentController = async (req, res, next) => {
    // Obtener el ID de la recomendación desde los parámetros de la solicitud
    const id = req.params.id;

    try {
        // Obtener una conexión a la base de datos
        const pool = await getPool();

        // Consulta SQL para obtener la lista de comentarios de la recomendación especificada
        const [commentList] = await pool.query(
            `SELECT id, Comment, vote, createAt
        FROM comment
        WHERE recommendation_id = ?
        ORDER BY createAt DESC
        `,
            [id]
        );
        // Enviar una respuesta con la lista de comentarios
        res.send({ data: commentList });
    } catch (error) {
        next(error);
    }
};

module.exports = listCommentController;
