// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Controlador para listar recomendaciones con filtros de lugar y categoría
const recommendationListController = async (req, res, next) => {
    try {
        // Obtenemos los posibles filtros de los query params.
        let { place, category } = req.query;

        // Si no recibimos ningún valor en los filtros establecemos un string vacío.
        place = place || '';
        category = category || '';

        const pool = await getPool();

        // Consulta SQL para obtener recomendaciones que coincidan con los filtros
        const [recommendation] = await pool.query(
            `
                SELECT 
                    r.id, 
                    r.title, 
                    r.place, 
                    r.category, 
                    r.recommendation, 
                    r.photo, 
                    r.createAt, 
                    u.name, 
                    AVG(IFNULL(c.vote, 0)) AS mediaVotos
                FROM recommendation AS r
                INNER JOIN users AS u ON (u.id = r.users_id)
                LEFT JOIN comment AS c ON (r.id = c.recommendation_id)
                WHERE r.place LIKE ? AND r.category LIKE ?
                GROUP BY r.id
                ORDER BY r.createAt DESC;
            `,
            [`%${place}%`, `%${category}%`]
        );

        // Enviar una respuesta con las recomendaciones que coincidan con los filtros
        res.send({ data: recommendation });
    } catch (error) {
        next(error);
    }
};

module.exports = recommendationListController;
