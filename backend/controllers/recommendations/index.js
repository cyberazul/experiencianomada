const addCommentController = require('./addCommentController');
const recommendationListController = require('./recommendationListController');
const deleteRecommendationController = require('./deleteRecommendationController');
const listCommentController = require('./listCommentController');
const addRecomController = require('./addRecomController');
const detailRecommendationController = require('./detailRecommendationController');

const {
    getAscendientesRecommendations,
    getDescendientesRecommendations
} = require('./ordRecomController');

module.exports = {
    recommendationListController,
    addCommentController,
    deleteRecommendationController,
    listCommentController,
    addRecomController,
    detailRecommendationController,
    getAscendientesRecommendations,
    getDescendientesRecommendations
};
