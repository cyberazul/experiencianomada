// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Controlador para obtener recomendaciones ordenadas de manera ascencente
const getAscendientesRecommendations = async (req, res, next) => {
    try {
        const pool = await getPool();

        // Consulta SQL para obtener recomendaciones
        const [recommendation] = await pool.query(`
        SELECT r.id, r.title, r.place, r.category, r.recommendation, r.photo, r.createAt, u.name, AVG(IFNULL(c.vote, 0)) AS mediaVotos
        FROM recommendation AS r
        INNER JOIN users AS u ON (u.id = r.Users_id)
        LEFT JOIN comment AS c ON (r.id = c.recommendation_id)
        GROUP BY r.id
        ORDER BY mediaVotos ASC;
    `);
        // Enviar una respuesta con las recomendaciones ordenadas
        res.send({ data: recommendation });
    } catch (error) {
        next(error);
    }
};

// Controlador para obtener recomendaciones ordenadas de manera ascencente
async function getDescendientesRecommendations(req, res, next) {
    try {
        const pool = await getPool();

        // Consulta SQL para obtener recomendaciones
        const [recommendation] = await pool.query(`
        SELECT r.id, r.title, r.place, r.category, r.recommendation, r.photo, r.createAt, u.name, AVG(IFNULL(c.vote, 0)) AS mediaVotos
        FROM recommendation AS r
        INNER JOIN users AS u ON (u.id = r.Users_id)
        LEFT JOIN comment AS c ON (r.id = c.recommendation_id)
        GROUP BY r.id
        ORDER BY mediaVotos DESC;
    `);

        // Enviar una respuesta con las recomendaciones ordenadas
        res.send({ data: recommendation });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAscendientesRecommendations,
    getDescendientesRecommendations
};
