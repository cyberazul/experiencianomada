// Importamos la función que me permite conectarme con la base de datos.
const getPool = require('../../db/getPool');

// Importamos helper que crea un path si no existe ya.
const createPathIfNotExists = require('../../utils/createPathIfNotExists');

// Importamos sharp para poder modificar el tamaño de la imagen.
const sharp = require('sharp');

// Importamos nanoid para renombrar la imagen con un nombre aleatorio.
const { nanoid } = require('nanoid');

// Importamos path para que las rutas sean válidas.
const path = require('path');

// Importamos el esquema.
const addRecomSchema = require('../../schemas/recommendations/addRecomSchema');

// Importamos la función que valida esquemas.
const validateSchema = require('../../utils/validateSchema');

// Controlador para agregar una nueva recomendación
const addRecomController = async (req, res, next) => {
    try {
        // Validar los datos de la solicitud
        await validateSchema(addRecomSchema, req.body);

        // Extraer datos del cuerpo de la solicitud
        const { title, place, text, category } = req.body;

        let imageFileName;

        // Comprobar si la solicitud contiene archivos y si hay una imagen
        if (req.files && req.files.image) {
            const uploadsDir = path.join(__dirname, '../../uploads');

            // Crear el directorio de carga si no existe
            await createPathIfNotExists(uploadsDir);

            // Redimensionar la imagen
            const image = sharp(req.files.image.data);
            image.resize(300);

            // Generar un nombre de archivo único para la imagen
            imageFileName = `${nanoid(24)}.jpg`;

            // Guardar la imagen en el servidor
            await image.toFile(path.join(uploadsDir, imageFileName));
        }

        // Obtener una conexión a la base de datos
        const connection = await getPool();

        // Ejecutar una consulta SQL que inserta datos en la tabla
        const [results] = await connection.query(
            `
            INSERT INTO recommendation (title, place, category, recommendation, photo, Users_id)
            VALUES (?, ?, ?, ?, ?, ?)

            `,
            [title, place, category, text, imageFileName, req.userId]
        );

        res.status(200).send({
            ok: true,
            data: null,
            message: 'Recomendación creada correctamente.'
        });

        return results.insertId;
    } catch (error) {
        return next(error);
    }
};

module.exports = addRecomController;
