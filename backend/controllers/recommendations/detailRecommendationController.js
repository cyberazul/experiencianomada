// Importamos la función que genera una conexión con la base de datos.
const getPool = require('../../db/getPool');

// Importamos la función que genera errores.
const createError = require('../../utils/createError');

// Controlador para obtener detalles de una recomendación
const detailRecommendationController = async (req, res, next) => {
    try {
        // Obtener el ID de la recomendación desde los parámetros de la solicitud
        const { id } = req.params;

        // Obtener una conexión a la base de datos
        const pool = await getPool();

        // Consulta SQL para obtener detalles de la recomendación
        const [recommendations] = await pool.query(
            `
                SELECT 
                    r.id, 
                    r.title, 
                    r.place, 
                    r.category, 
                    r.recommendation, 
                    r.photo, 
                    r.createAt, 
                    u.name, 
                    AVG(IFNULL(c.vote, 0)) AS mediaVotos
                FROM recommendation AS r 
                INNER JOIN users AS u ON (u.id = r.Users_id)
                LEFT JOIN comment AS c ON (r.id = c.recommendation_id)
                WHERE r.id = ?
                GROUP BY r.id
                ORDER BY r.createAt DESC
            `,
            [id]
        );

        // Comprobar si se encontraron recomendaciones
        if (recommendations.length < 1) {
            createError(404, 'Recomendación no encontrada');
        }

        // Buscamos los comentarios de la recomendación seleccionada.
        const [comments] = await pool.query(
            `
            SELECT c.id, c.comment, c.createAt, c.vote, u.name AS userName
            FROM comment AS c
            INNER JOIN users AS u ON (u.id = c.Users_id)
            WHERE c.recommendation_id = ?
            `,
            [id]
        );

        res.send({
            status: 'ok',
            data: {
                ...recommendations[0],
                comments
            }
        });
    } catch (error) {
        next(error);
    }
};

module.exports = detailRecommendationController;
