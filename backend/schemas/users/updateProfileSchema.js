const Joi = require('joi');

// Definición del esquema Joi para validar una actualización de perfil de usuario
const updateProfileSchema = Joi.object({
    name: Joi.string().min(5).max(200),
    email: Joi.string().email(),
    password: Joi.string()
        .min(8)
        .pattern(
            new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])')
        )
});

module.exports = updateProfileSchema;
