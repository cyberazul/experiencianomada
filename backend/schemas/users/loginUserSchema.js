const Joi = require('joi');

// Definición del esquema Joi para validar un login de usuario
const loginSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required()
});

module.exports = loginSchema;
