const Joi = require('joi');

// Definición del esquema Joi para validar un registro de nuevo usuario
const newUserSchema = Joi.object({
    name: Joi.string().min(5).max(200).required(),
    email: Joi.string().email().required(),
    password: Joi.string()
        .min(8)
        .pattern(
            new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])')
        )
        .required()
});

module.exports = newUserSchema;
