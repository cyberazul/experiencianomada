// schemas.js
const Joi = require('joi');

// Definición del esquema Joi para validar una nueva recomendación
const addRecomSchema = Joi.object({
    title: Joi.string().min(5).max(50).required(),
    place: Joi.string().min(3).max(50).required(),
    text: Joi.string().required(),
    category: Joi.string().required()
});

module.exports = addRecomSchema;
