const Joi = require('joi');

// Definición del esquema Joi para validar un nuevo comentario
const newCommentSchema = Joi.object({
    Comment: Joi.string().min(5).max(255).required(),
    vote: Joi.number().integer().min(1).max(5).required()
});

module.exports = newCommentSchema;
